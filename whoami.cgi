#!/usr/bin/env ruby

# Reflection page CGI program
# Daniel Bowling <swaggboi@slackware.uk>
# Oct 2020

require 'cgi'

# Create CGI object
cgi = CGI.new('html4')

# Style sheet hash
styling = {
  rel:  'stylesheet',
  type: 'text/css',
  href: '/css/swagg.css'
}

# Div hashes
inner = { class: 'inner' }
outer = { class: 'outer' }

# Handle X-Forwarded-For header
x_forwarded_for = ENV['HTTP_X_FORWARDED_FOR'].split(/, ?/).first \
  if ENV['HTTP_X_FORWARDED_FOR'] =~ /,/

# CGI Environment variables
referer         = cgi.referer                  || nil
user_agent      = cgi.user_agent               || nil
remote_addr     = x_forwarded_for              || cgi.remote_addr    || nil
remote_port     = ENV['HTTP_X_FORWARDED_PORT'] || ENV['REMOTE_PORT'] || nil
request_method  = cgi.request_method           || nil
server_admin    = ENV['SERVER_ADMIN']          || nil
server_protocol = cgi.server_protocol          || nil
server_software = cgi.server_software          || nil

# Environment hash
cgi_env = {
  'referer'   => referer,
  'string'    => user_agent,
  'address'   => remote_addr,
  'port'      => remote_port,
  'method'    => request_method,
  'webmaster' => 'swaggboi@slackware.uk',
  'protocol'  => server_protocol,
  'server'    => server_software
}

# Footer
footer = "<footer>Return to #{cgi.a('/') { 'homepage' }}</footer>\n"
# Easter egg hash
easter_egg = {
  src:    '/Pictures/oprahv6.jpg',
  alt:    'Oprah screaming that everyone gets a /48',
  width:  400,
  height: 300
}
# Easter egg for IPv6
footer =
  if cgi_env['address'] =~ /[a-fA-F:]/
    cgi.img(easter_egg) + cgi.br + "\n" + cgi.br + "\n" + footer
  else
    '<iframe title="Jackie chan who am I roar! video"
             width="560"
             height="315"
             src="https://www.youtube-nocookie.com/embed/QOQ2JxIddzw"
             frameborder="0" allow="accelerometer;
                                    autoplay;
                                    clipboard-write;
                                    encrypted-media;
                                    gyroscope;
                                    picture-in-picture"
             allowfullscreen>
     </iframe>' + cgi.br + "\n" + footer
  end

# Begin HTML output
cgi.out do
  # Open html tag
  "\n" + cgi.html do
    # Open head tag
    "\n" + cgi.head do
      # Title tag
      "\n" + cgi.title { 'Reflection Page' } + "\n" +
      # Link tag
      cgi.link(styling) + "\n"
    # Close head tag
    end + "\n" +
    # Open body tag
    cgi.body do
      "\n" + cgi.div(outer) do
        # H1 heading
        "\n" + cgi.h1 { 'Swagg::Net Reflection Page' }
      end + "\n" +
      # Open div tag (class="inner")
      cgi.div(inner) do
        # Begin table
        "\n" + cgi.table do
          # Referer row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'Your referer: ' } + "\n" +
            cgi.td { cgi_env['referer'] } + "\n"
          end +
          # User-agent string row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'Your user-agent string: ' } + "\n" +
            cgi.td { cgi_env['string'] } + "\n"
          end +
          # IP address row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'Your IP address: ' } + "\n" +
            cgi.td { cgi_env['address'] } + "\n"
          end +
          # Port row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'Your TCP port: ' } + "\n" +
            cgi.td { cgi_env['port'] } + "\n"
          end +
          # Request method row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'Your request method: ' } + "\n" +
            cgi.td { cgi_env['method'] } + "\n"
          end +
          # Webmaster row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'My webmaster: ' } + "\n" +
            cgi.td { cgi_env['webmaster'] } + "\n"
          end +
          # Server protocol row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'My protocol: ' } + "\n" +
            cgi.td { cgi_env['protocol'] } + "\n"
          end +
          # Server software row
          "\n" + cgi.tr do
            "\n" + cgi.th { 'My server: ' } + "\n" +
            cgi.td { cgi_env['server'] } + "\n"
          end + "\n"
        # End table
        end + "\n" +
        cgi.br + "\n" +
        # Footer
        footer
      # Close div tag (class="inner")
      end + "\n"
    # Close body tag
    end + "\n"
  # Close html tag
  end + "\n"
# End HTML output
end
